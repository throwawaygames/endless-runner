﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scoring : MonoBehaviour {
	public int score;
	public bool lockout;
	// Use this for initialization
	void Start () {
		score = 0;
	}
	void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "building")
		{
			if (lockout == false) {
				score += 1;
				Debug.Log (score);
				lockout = true;
				StartCoroutine (lockoutf ());
			}


		}
	}
	IEnumerator lockoutf()
	{
		yield return new WaitForSeconds (2);
		lockout = false;
	}
}